﻿using System.Windows;
using PatientsList.WpfClient.ViewModels;

namespace PatientsList.WpfClient.Windows
{
    /// <summary>
    /// Interaction logic for EditPatientWindow.xaml
    /// </summary>
    public partial class EditPatientWindow : Window
    {
        public EditPatientWindow()
        {
            this.InitializeComponent();
        }

        public PatientViewModel Patient
        {
            get { return (PatientViewModel)this.DataContext; }
            set { this.DataContext = value; }
        }
    }
}
