﻿using System.Windows;
using Autofac;
using PatientsList.WpfClient.ViewModels;

namespace PatientsList.WpfClient.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.InitializeComponent();

            var container = ((App)Application.Current).Container;
            var viewModel = container.Resolve<PatientsListViewModel>();
            
            this.DataContext = viewModel;
        }
    }
}
