﻿using System;
using System.Text;
using System.Windows;
using System.Windows.Input;
using PatientsList.BusinessLogic.Dtos;
using PatientsList.BusinessLogic.Services;
using PatientsList.Infrastructure;
using PatientsList.Infrastructure.Mvvm;

namespace PatientsList.WpfClient.ViewModels
{
    public class PatientViewModel : ObservableObject
    {
        public PatientDto Patient { get; set; }

        private readonly PatientsService patientsService;

        public PatientViewModel(PatientsService patientsService)
        {
            this.patientsService = patientsService;
        }


        public string Pesel
        {
            get { return this.Patient.PatientID; }
            set { this.SetPropertyValue<PatientDto>(x => x.PatientID, this.Patient, value); }
        }

        public string Name
        {
            get { return this.Patient.PatientNameGroup1; }
            set { this.SetPropertyValue<PatientDto>(x => x.PatientNameGroup1, this.Patient, value); }
        }

        public string LastName
        {
            get { return this.Patient.PatientNameGroup2; }
            set { this.SetPropertyValue<PatientDto>(x => x.PatientNameGroup2, this.Patient, value); }
        }

        public string Sex
        {
            get { return this.Patient.PatientSex; }
            set { this.SetPropertyValue<PatientDto>(x => x.PatientSex, this.Patient, value); }
        }

        public DateTime? BirthDate
        {
            get { return this.Patient.PatientBirthDate; }
            set
            {
                if (this.Patient.PatientBirthDate != value)
                {
                    this.Patient.PatientBirthDate = value;
                    RaisePropertyChanged(ObjectHelper.GetPropertyName<PatientDto>(p => p.PatientBirthDate));
                }
            }
        }

        public void SaveChangesExecute()
        {
            if (this.Patient.PatientInfoID == 0)
            {
                var result = this.patientsService.SaveNew(this.Patient);
                this.ShowErrorOrCloseWindow(result);
            }
            else
            {
                var result = this.patientsService.Update(this.Patient);
                this.ShowErrorOrCloseWindow(result);
            }
        }

        private void ShowErrorOrCloseWindow(OperationResult result)
        {
            if (!result.Succed)
            {
                var sb = new StringBuilder();
                foreach (var error in result.Errors)
                {
                    sb.AppendLine(error);
                }

                MessageBox.Show(sb.ToString(), "Błąd walidacji", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (this.Window != null)
            {
                this.Window.Close();
            }
        }

        public ICommand SaveChanges { get { return new RelayCommand(this.SaveChangesExecute, () => true); } }

        public Window Window { get; set; }
    }
}