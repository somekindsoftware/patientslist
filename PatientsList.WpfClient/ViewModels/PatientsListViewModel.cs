﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;
using Autofac;
using PatientsList.BusinessLogic.Dtos;
using PatientsList.BusinessLogic.Paging;
using PatientsList.BusinessLogic.Services;
using PatientsList.Infrastructure;
using PatientsList.Infrastructure.Mvvm;
using PatientsList.WpfClient.Windows;

namespace PatientsList.WpfClient.ViewModels
{
    public class PatientsListViewModel : ObservableObject
    {
        private readonly PatientsService patientsService;

        private ObservableCollection<PatientDto> patients;

        public ObservableCollection<PatientDto> Patients
        {
            get { return this.patients; }
            set
            {
                if (this.patients != value)
                {
                    this.patients = value;
                    RaisePropertyChanged(ObjectHelper.GetPropertyName<PatientsListViewModel>(p => p.Patients));
                }
            }
        }

        private PatientDto selectedPatient;

        public PatientDto SelectedPatient
        {
            get { return this.selectedPatient; }
            set
            {
                if (this.selectedPatient != value)
                {
                    this.selectedPatient = value;
                    RaisePropertyChanged(ObjectHelper.GetPropertyName<PatientsListViewModel>(p => p.SelectedPatient));
                }
            }
        }

        public PatientsListViewModel(PatientsService patientsService)
        {
            this.patientsService = patientsService;
            this.ReloadData();
        }


        public ICommand DeletePatient { get { return new RelayCommand(this.DeletePatientExecute, this.IsPatientSelected); } }

        public void DeletePatientExecute()
        {
            this.patientsService.Delete(this.SelectedPatient.PatientInfoID);
            this.ReloadData();
        }

        public ICommand AddNewPatient { get { return new RelayCommand(() => this.OpenEditPatientWindow(new PatientDto()), () => true); } }

        public ICommand EditPatient { get { return new RelayCommand(() => this.OpenEditPatientWindow(this.SelectedPatient), () => this.IsPatientSelected()); } }

        private void OpenEditPatientWindow(PatientDto patient)
        {
            var container = ((App)Application.Current).Container;
            var viewModel = container.Resolve<PatientViewModel>();

            viewModel.Patient = patient;

            var window = new EditPatientWindow();
            window.Patient = viewModel;
            window.Closed += this.EditPatientWindow_Closed;
            viewModel.Window = window;

            window.ShowDialog();
        }

        private void EditPatientWindow_Closed(object sender, EventArgs e)
        {
            this.ReloadData();
        }

        private void ReloadData()
        {
            var result = this.patientsService.GetPatients(new DataPageRequest(1, 10));
            this.Patients = new ObservableCollection<PatientDto>(result.Items);
        }

        public bool IsPatientSelected()
        {
            return this.SelectedPatient != null;
        }
    }
}
