﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Windows;
using Autofac;
using Autofac.Core;

namespace PatientsList.WpfClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public IContainer Container { get; private set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            AppDomain.CurrentDomain.Load("PatientsList.DataLayer");
            AppDomain.CurrentDomain.Load("PatientsList.BusinessLogic");

            var builder = new ContainerBuilder();

            builder.RegisterAssemblyModules(AppDomain.CurrentDomain.GetAssemblies());

            if (this.Container == null)
            {
                this.Container = builder.Build();
            }
            else
            {
                builder.Update(this.Container);
            }

            this.ShutdownMode = ShutdownMode.OnMainWindowClose;
        }

        protected override void OnExit(ExitEventArgs e)
        {
            this.Container.Dispose();
            this.Container = null;
        }

    }
}
