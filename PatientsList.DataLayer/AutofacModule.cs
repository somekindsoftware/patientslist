﻿using Autofac;

namespace PatientsList.DataLayer
{
    class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new TestEntities()).AsSelf().InstancePerLifetimeScope();
        }
    }
}
