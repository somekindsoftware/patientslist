﻿using FluentValidation;
using PatientsList.BusinessLogic.Dtos;

namespace PatientsList.BusinessLogic.Validation
{
    class PatientValidator : AbstractValidator<PatientDto>
    {
        public PatientValidator()
        {
            this.RuleFor(x => x.PatientBirthDate).NotEmpty().WithMessage("Data nie może być pusta");
            this.RuleFor(x => x.PatientID).NotEmpty().WithMessage("PESEL nie może być pusty");
            this.RuleFor(x => x.PatientNameGroup1).NotEmpty().WithMessage("Imię nie może być puste");
            this.RuleFor(x => x.PatientNameGroup2).NotEmpty().WithMessage("Nazwisko nie może być puste");
            this.RuleFor(x => x.PatientSex).NotEmpty().WithMessage("Płeć nie może być pusta");
        }
    }
}
