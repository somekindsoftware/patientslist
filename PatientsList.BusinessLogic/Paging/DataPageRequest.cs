﻿namespace PatientsList.BusinessLogic.Paging
{
    public sealed class DataPageRequest
    {
        public int PageNumber { get; private set; }
        public int PageSize { get; private set; }

        public DataPageRequest(int currentPageNumber, int pageSize)
        {
            this.PageNumber = currentPageNumber;
            this.PageSize = pageSize;
        }
    }
}