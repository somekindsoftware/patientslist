﻿using System;

namespace PatientsList.BusinessLogic.Paging
{
    public sealed class PagingInfo
    {
        public int PageNumber { get; private set; }
        public int PageSize { get; private set; }
        public int TotalItemsCount { get; private set; }

        public int TotalPages
        {
            get { return (int)Math.Ceiling((decimal)this.TotalItemsCount / this.PageSize); }
        }

        public PagingInfo(int pageNumber, int pageSize, int totalItemsCount)
        {
            this.PageNumber = pageNumber;
            this.TotalItemsCount = totalItemsCount;
            this.PageSize = pageSize;
        }
    }
}