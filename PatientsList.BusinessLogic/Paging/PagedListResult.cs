﻿using System.Collections.Generic;

namespace PatientsList.BusinessLogic.Paging
{
    public sealed class PagedListResult<T>
    {
        public IEnumerable<T> Items { get; private set; }
        public PagingInfo Configuration { get; private set; }

        public PagedListResult(IEnumerable<T> items, int pageNumber, int pageSize, int totalItemsCount)
        {
            this.Items = items;
            this.Configuration = new PagingInfo(pageNumber, pageSize, totalItemsCount);
        }
    }
}