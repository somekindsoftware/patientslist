﻿using System.Reflection;
using Autofac;
using Autofac.Extras.DynamicProxy2;
using PatientsList.DataLayer;
using PatientsList.Infrastructure.EntityFramework;

namespace PatientsList.BusinessLogic
{
    public class AutofacModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(AutoSavingChangesInterceptor<>)).AsSelf();

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.Name.EndsWith("Service"))
                .EnableClassInterceptors()
                .InterceptedBy(typeof(AutoSavingChangesInterceptor<TestEntities>));
        }
    }
}
