﻿using System.Linq;
using FluentValidation.Results;
using PatientsList.BusinessLogic.Dtos;
using PatientsList.BusinessLogic.Paging;
using PatientsList.BusinessLogic.Validation;
using PatientsList.DataLayer;
using PatientsList.Infrastructure.EntityFramework;
using PatientsList.Infrastructure.ValueInjection;

namespace PatientsList.BusinessLogic.Services
{
    public class PatientsService : ServiceBase
    {
        public PatientsService(TestEntities context)
        {
            this.context = context;
        }
        
        public PagedListResult<PatientDto> GetPatients(DataPageRequest request)
        {
            var items = this.context.PatientInfos
                .OrderBy(x => x.PatientInfoID)
                .Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize);

            var totalItemsCount = this.context.PatientInfos.Count();

            var dtos = Injector.CreateAndInject<PatientInfo, PatientDto>(items);

            return new PagedListResult<PatientDto>(dtos, request.PageNumber, request.PageSize, totalItemsCount);
        }

        [AutoSave]
        public virtual OperationResult SaveNew(PatientDto patient)
        {
            ValidationResult vr = new PatientValidator().Validate(patient);
            if (!vr.IsValid)
            {
                return this.CreateOperationResult(vr);
            }

            var patientInfo = Injector.CreateAndInject<PatientDto, PatientInfo>(patient);
            this.context.PatientInfos.AddObject(patientInfo);
            return new OperationResult(true);
        }

        [AutoSave]
        public virtual OperationResult Update(PatientDto patient)
        {
            ValidationResult vr = new PatientValidator().Validate(patient);
            if (!vr.IsValid)
            {
                return this.CreateOperationResult(vr);
            }

            var patientInfo = this.context.PatientInfos.Single(x => x.PatientInfoID == patient.PatientInfoID);
            patientInfo.InjectFrom(patient);
            return new OperationResult(true);
        }

        [AutoSave]
        public virtual void Delete(int id)
        {
            var patientInfo = this.context.PatientInfos.Single(x => x.PatientInfoID == id);
            this.context.DeleteObject(patientInfo);
        }
    }
}

