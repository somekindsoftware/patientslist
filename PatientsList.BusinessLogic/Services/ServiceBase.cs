﻿using System;
using System.Linq;
using FluentValidation.Results;
using PatientsList.DataLayer;

namespace PatientsList.BusinessLogic.Services
{
    public class ServiceBase : IDisposable
    {
        protected TestEntities context;
        private bool disposed;

        public OperationResult CreateOperationResult(ValidationResult validationResult)
        {
            var result = new OperationResult(false);
            result.Errors = validationResult.Errors.Select(x => x.ErrorMessage).ToList();
            return result;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (this.context != null)
                    {
                        this.context.Dispose();
                        this.context = null;
                    }
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}