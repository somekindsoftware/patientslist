﻿using System.Collections.Generic;

namespace PatientsList.BusinessLogic.Services
{
    public class OperationResult
    {
        public bool Succed { get; set; }
        public List<string> Errors { get; set; }

        public OperationResult(bool succed)
        {
            this.Succed = succed;
        }
    }
}