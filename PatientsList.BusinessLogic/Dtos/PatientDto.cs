﻿using System;

namespace PatientsList.BusinessLogic.Dtos
{
    public class PatientDto
    {
        public int PatientInfoID { get; set; }
        public string PatientID { get; set; }
        public string PatientNameGroup1 { get; set; }
        public string PatientNameGroup2 { get; set; }
        public DateTime? PatientBirthDate { get; set; }
        public string PatientSex { get; set; }
    }
}
