﻿using System;
using System.Reflection;
using Autofac;
using Autofac.Extras.DynamicProxy2;
using NLog;
using NLog.Config;
using NLog.Targets;
using NUnit.Framework;
using PatientsList.DataLayer;
using PatientsList.Infrastructure.EntityFramework;

namespace PatientsList.Tests.Integration
{
    [TestFixture]
    public abstract class ServiceIntegrationTestsBase
    {
        protected Autofac.IContainer Container;

        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            this.BuildContainerForFixture();
            this.ConfigureLogManager();
        }

        private void BuildContainerForFixture()
        {
            var builder = new ContainerBuilder();

            builder.RegisterGeneric(typeof(AutoSavingChangesInterceptor<>)).AsSelf();

            builder.RegisterAssemblyTypes(Assembly.Load("PatientsList.BusinessLogic"))
                .Where(t => t.Name.EndsWith("Service"))
                .EnableClassInterceptors()
                .InterceptedBy(typeof(AutoSavingChangesInterceptor<TestEntities>));

            this.Container = builder.Build();
        }

        private void ConfigureLogManager()
        {
            var console = new ColoredConsoleTarget();
            console.Layout = @"${date:format=HH\\:MM\\:ss} ${logger} ${message}";

            var normal = new FileTarget();
            normal.Layout = "${longdate}|${level:uppercase=true}|${callsite}|${newline}${message}${newline}";
            normal.FileName = "${basedir}/normallog.txt";

            var error = new FileTarget();
            error.Layout = "${message}";
            error.FileName = "${newline}${longdate}|${level:uppercase=true}|${callsite}|${newline}${message}${newline}${exception:innerFormat=Message,Type,StackTrace:format=Message,Type,StackTrace:maxInnerExceptionLevel=10}${newline}";

            var config = new LoggingConfiguration();
            config.AddTarget("console", console);
            config.AddTarget("file", normal);
            config.AddTarget("error", error);

            config.LoggingRules.Add(new LoggingRule("*", LogLevel.Trace, console));
            config.LoggingRules.Add(new LoggingRule("*", LogLevel.Trace, normal));
            config.LoggingRules.Add(new LoggingRule("*", LogLevel.Trace, error));

            LogManager.Configuration = config;
        }

        [SetUp]
        public void TestSetup()
        {
            this.UpdateContainer();
            this.AdditionalSetUp();
        }

        private void UpdateContainer()
        {
            var builder = new ContainerBuilder();
            builder.Register(c => new TestEntities()).AsSelf().InstancePerLifetimeScope();
            builder.Update(this.Container);
        }

        protected virtual void AdditionalSetUp()
        {
        }

        protected T GetInNewContext<T>(Func<TestEntities, T> action)
        {
            T result;
            using (var tempContext = new TestEntities())
            {
                result = action(tempContext);
            }

            return result;
        }

        protected void SaveInNewContext(Action<TestEntities> action)
        {
            using (var tempContext = new TestEntities())
            {
                action(tempContext);
                tempContext.SaveChanges();
            }
        }

        protected void ExecuteInNewContext(Action<TestEntities> action)
        {
            using (var tempContext = new TestEntities())
            {
                action(tempContext);
                tempContext.SaveChanges();
            }
        }
    }
}