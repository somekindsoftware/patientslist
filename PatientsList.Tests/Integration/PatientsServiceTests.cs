﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using NUnit.Framework;
using PatientsList.BusinessLogic.Dtos;
using PatientsList.BusinessLogic.Paging;
using PatientsList.BusinessLogic.Services;
using PatientsList.DataLayer;

namespace PatientsList.Tests.Integration
{
    [TestFixture]
    public class PatientsServiceTests : ServiceIntegrationTestsBase
    {
        private PatientsService _service;

        protected override void AdditionalSetUp()
        {
            this._service = Container.Resolve<PatientsService>();

            ExecuteInNewContext(c => c.ExecuteStoreCommand("TRUNCATE TABLE PatientInfo"));
        }

        [Test]
        public void GetAll__TableIsEmpty__Success()
        {
            var result = this._service.GetPatients(new DataPageRequest(1, 10));

            Assert.That(result.Items.Count(), Is.EqualTo(0));
            Assert.That(result.Configuration.PageNumber, Is.EqualTo(1));
            Assert.That(result.Configuration.PageSize, Is.EqualTo(10));
            Assert.That(result.Configuration.TotalItemsCount, Is.EqualTo(0));
        }

        [Test]
        public void GetAll__TableIsNotEmpty__Success()
        {
            var patientInfosToSave = new List<PatientInfo> {
                    new PatientInfo { PatientNameGroup1 = "A" },
                    new PatientInfo { PatientNameGroup1 = "B" },
                    new PatientInfo { PatientNameGroup1 = "C" },
                    new PatientInfo { PatientNameGroup1 = "D" },
                    new PatientInfo { PatientNameGroup1 = "E" },
            };

            this.SaveInNewContext(c => {
                foreach (var patientInfo in patientInfosToSave)
                {
                    c.PatientInfos.AddObject(patientInfo);
                }
            });

            var result = this._service.GetPatients(new DataPageRequest(1, 3));

            Assert.That(result.Items.Count(), Is.EqualTo(3));
            Assert.That(result.Configuration.PageNumber, Is.EqualTo(1));
            Assert.That(result.Configuration.PageSize, Is.EqualTo(3));
            Assert.That(result.Configuration.TotalItemsCount, Is.EqualTo(5));
            Assert.That(result.Configuration.TotalPages, Is.EqualTo(2));
        }

        [Test]
        public void SaveNew__ValidPatientData__Success()
        {
            var patientDto = new PatientDto {
                PatientID = "123456789",
                PatientBirthDate = DateTime.Now.AddYears(-20),
                PatientNameGroup1 = "Jan",
                PatientNameGroup2 = "Kowalski",
                PatientSex = "M",
            };

            this._service.SaveNew(patientDto);

            var patientInfo = this.GetInNewContext(c => c.PatientInfos.SingleOrDefault(p => p.PatientID == patientDto.PatientID));

            Assert.That(patientInfo, Is.Not.Null);
            Assert.That(patientInfo.PatientNameGroup1, Is.EqualTo(patientDto.PatientNameGroup1));
            Assert.That(patientInfo.PatientNameGroup2, Is.EqualTo(patientDto.PatientNameGroup2));
            Assert.That(patientInfo.PatientSex, Is.EqualTo(patientDto.PatientSex));
            Assert.That(patientInfo.PatientBirthDate.HasValue, Is.True);
            Assert.That(patientInfo.PatientBirthDate.Value.Date, Is.EqualTo(patientDto.PatientBirthDate.Value.Date));
        }

        [Test]
        public void Update__ValidPatientData__Success()
        {
            var patientInfoToSave = new PatientInfo {
                PatientID = "test",
                PatientBirthDate = new DateTime(1900, 1, 1),
                PatientNameGroup1 = "test",
                PatientNameGroup2 = "test",
                PatientSex = "test"
            };

            this.SaveInNewContext(c => c.PatientInfos.AddObject(patientInfoToSave));

            var patientDto = new PatientDto  {
                PatientInfoID = patientInfoToSave.PatientInfoID,
                PatientID = "123456789",
                PatientBirthDate = DateTime.Now.AddYears(-20),
                PatientNameGroup1 = "Jan",
                PatientNameGroup2 = "Kowalski",
                PatientSex = "M",
            };

            this._service.Update(patientDto);

            var patientInfo = this.GetInNewContext(c => c.PatientInfos.SingleOrDefault(p => p.PatientID == patientDto.PatientID));

            Assert.That(patientInfo, Is.Not.Null);
            Assert.That(patientInfo.PatientNameGroup1, Is.EqualTo(patientDto.PatientNameGroup1));
            Assert.That(patientInfo.PatientNameGroup2, Is.EqualTo(patientDto.PatientNameGroup2));
            Assert.That(patientInfo.PatientSex, Is.EqualTo(patientDto.PatientSex));
            Assert.That(patientInfo.PatientBirthDate.HasValue, Is.True);
            Assert.That(patientInfo.PatientBirthDate.Value.Date, Is.EqualTo(patientDto.PatientBirthDate.Value.Date));
        }

        [Test]
        public void Delete__Success()
        {
            var patientInfoToSave = new PatientInfo
            {
                PatientID = "test",
                PatientBirthDate = new DateTime(1900, 1, 1),
                PatientNameGroup1 = "test",
                PatientNameGroup2 = "test",
                PatientSex = "test"
            };

            this.SaveInNewContext(c => c.PatientInfos.AddObject(patientInfoToSave));

            var patientDto = new PatientDto
            {
                PatientInfoID = patientInfoToSave.PatientInfoID,
                PatientID = "123456789",
                PatientBirthDate = DateTime.Now.AddYears(-20),
                PatientNameGroup1 = "Jan",
                PatientNameGroup2 = "Kowalski",
                PatientSex = "M",
            };

            this._service.Delete(patientDto.PatientInfoID);

            var patientInfo = this.GetInNewContext(c => c.PatientInfos.SingleOrDefault(p => p.PatientID == patientDto.PatientID));

            Assert.That(patientInfo, Is.Null);
        }
    }
}
