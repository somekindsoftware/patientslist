using System.Collections.Generic;
using System.Linq;

namespace PatientsList.Infrastructure.ValueInjection
{
    public static class Injector
    {
        public static TTarget CreateAndInject<TSource, TTarget>(TSource source)
            where TTarget : new()
        {
            var target = new TTarget();
            target.InjectFrom(source);
            return target;
        }

        public static IEnumerable<TTarget> CreateAndInject<TSource, TTarget>(IEnumerable<TSource> sourceItems)
            where TTarget : new()
        {
            return sourceItems.Select(CreateAndInject<TSource, TTarget>).ToList();
        }
    }
}