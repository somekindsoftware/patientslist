using Omu.ValueInjecter;

namespace PatientsList.Infrastructure.ValueInjection
{
    public static class ObjectInjectionExtensions
    {
        public static T InjectFrom<T>(this T target, object source)
        {
            target
                .InjectFrom<FlatLoopValueInjection>(source)
                .InjectFrom<UnflatLoopValueInjection>(source)
                ;

            return target;
        }
    }
}