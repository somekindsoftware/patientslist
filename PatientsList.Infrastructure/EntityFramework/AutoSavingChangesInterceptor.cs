﻿using System;
using System.Data.Objects;
using System.Reflection;
using Castle.DynamicProxy;
using NLog;

namespace PatientsList.Infrastructure.EntityFramework
{
    public class AutoSavingChangesInterceptor<T> : IInterceptor
        where T: ObjectContext
    {
        private readonly Logger logger = LogManager.GetCurrentClassLogger();

        private readonly T context;

        public AutoSavingChangesInterceptor(T context)
        {
            this.context = context;
        }

        public void Intercept(IInvocation invocation)
        {
            string methodName = string.Format("{0}.{1}", invocation.Method.DeclaringType.FullName, invocation.Method.Name);

            this.logger.Trace("AutoSavingChangesInterceptor dla metody: {0}", methodName);

            bool autoSaveEnabled = this.IsAutoSaveEnabled(invocation.Method);

            try
            {
                invocation.Proceed();

                if (autoSaveEnabled)
                {
                    this.logger.Trace("Zatwierdzanie transakcji");
                    this.context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                this.logger.ErrorException(string.Format("Błąd podczas zatwierdzania transakcji w metodzie: {0}", methodName), ex);
                throw;
            }
        }

        private bool IsAutoSaveEnabled(MethodInfo mi)
        {
            var classAttributes = mi.DeclaringType.GetCustomAttributes(typeof(AutoSaveAttribute), false);
            var methodAttributes = mi.GetCustomAttributes(typeof(AutoSaveAttribute), false);

            return classAttributes.Length > 0 || methodAttributes.Length > 0;
        }
    }
}
