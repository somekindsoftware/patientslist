﻿using System;

namespace PatientsList.Infrastructure.EntityFramework
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class AutoSaveAttribute : Attribute
    {
    }
}