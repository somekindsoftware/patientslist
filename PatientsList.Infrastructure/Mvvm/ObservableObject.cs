﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;

//Event Design: http://msdn.microsoft.com/en-us/library/ms229011.aspx

namespace PatientsList.Infrastructure.Mvvm
{
    [Serializable]
    public abstract class ObservableObject : INotifyPropertyChanged
    {
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected void RaisePropertyChanged<T>(Expression<Func<T>> propertyExpresssion)
        {
            var propertyName = PropertySupport.ExtractPropertyName(propertyExpresssion);
            this.RaisePropertyChanged(propertyName);
        }

        protected void RaisePropertyChanged(String propertyName)
        {
            this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        protected void SetPropertyValue<T>(Expression<Func<T, object>> expression, object target, object value)
        {
            var property = (PropertyInfo)((MemberExpression)expression.Body).Member;

            var originalValue = property.GetValue(target, null);

            if (originalValue != value)
            {
                property.SetValue(target, value, null);
                RaisePropertyChanged(ObjectHelper.GetPropertyName(expression));
            }
        }
    }
}
