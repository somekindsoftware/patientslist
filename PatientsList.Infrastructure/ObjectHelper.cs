using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace PatientsList.Infrastructure
{
    public class ObjectHelper
    {
        public static string GetMethodName<T>(Expression<Action<T>> expression)
        {
            var method = (MethodCallExpression)expression.Body;
            return method.Method.Name;
        }

        public static string GetPropertyName<T>(Expression<Func<T, object>> expression)
        {
            var body = expression.Body as MemberExpression;

            if (body == null)
            {
                body = ((UnaryExpression)expression.Body).Operand as MemberExpression;
            }

            return body.Member.Name;
        }

        public static IEnumerable<string> GetAllPropertiesNames<T>()
        {
            return typeof(T).GetProperties().Select(propertyInfo => propertyInfo.Name);
        }
    }
}